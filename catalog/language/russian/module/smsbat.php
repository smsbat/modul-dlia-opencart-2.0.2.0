<?php

$_['smsbat_saved_success'] = 'Настройки удачно сохранены';

$_['smsbat_message_connection_error'] = 'При отправке оповещения по SMS возникли неполадки со шлюзом SmsBat.com.<br />Время отправки: %s<br />Ответ сервера: %s';
$_['smsbat_message_customer_new_register'] = 'Поздравляем с успешной регистрацией в интернет-магазине "%s"';
$_['smsbat_message_customer_new_order'] = 'Спасибо за покупку. Ваш номер заказа #%s';
$_['smsbat_message_admin_new_customer'] = 'Зарегистрирован новый покупатель';
$_['smsbat_message_admin_new_order'] = 'Новый заказ';
$_['smsbat_message_admin_new_email'] = 'Отправлено новое письмо со страницы контактов';

$_['smsbat_page_head_title'] = 'Модуль SmsBat';

$_['smsbat_message_customer_new_order_status'] = 'Статус заказа #%s изменился на "%s".';